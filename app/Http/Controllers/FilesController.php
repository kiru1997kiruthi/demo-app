<?php

namespace App\Http\Controllers;

use App\Models\Files;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class FilesController extends Controller
{
    public function filesShowall()
    {
        $files = Files::join('categories', 'categories.id', '=', 'files.category_id')
            ->select('files.*', 'categories.category_name')
            ->get();
        $categories = DB::table('categories')->get();
        return view('pages.files_showall', compact('files', 'categories'));
    }

    public function filesStore(Request $request)
    {
        $request->validate([
            'title' => 'required',
            'category' => 'required',
            'file' => 'required|mimes:csv,xlx,xls,pdf,txt',
        ]);

        // file name setup
        $file_name = $request->title .time(). '.' . $request->file->extension();
        $file_name = trim($file_name);

        // create and update
        if ($request->id == null) {
            $file_name = trim($file_name);
            $file = new Files();
            $file->title = $request->title;
            $file->category_id = $request->category;
            $file->file_name = $file_name;
            $file->description = $request->description;
            $file->save();
            return redirect('/files-showall')->with('success','Successfully Recorded');
        } else {
            $request->file->move('files', $file_name);
            $file = Files::find($request->id);
            $file->title = $request->title;
            $file->category_id = $request->category;
            $file->file_name = $file_name;
            $file->description = $request->description;
            $file->save();
            return redirect('/files-showall')->with('success','Successfully Updated');
        }
    }
}
