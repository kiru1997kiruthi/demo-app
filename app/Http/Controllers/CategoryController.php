<?php

namespace App\Http\Controllers;

use App\Models\Category;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    // view all details
    public function categoryShowall()
    {
        $categories=Category::all();
        return view('pages.category_showall',compact('categories'));
    }

    // create and update
    public function categoryStore(Request $request)
    {
        if ($request->id == null) {
            $request->validate([
                'name' => 'required|unique:categories,category_name',
            ]);

            $category = new Category();
            $category->category_name = $request->name;
            $category->description = $request->description;
            $category->save();
            return redirect('/category-showall')->with('success','Successfully Recorded');
        } else {
            $request->validate([
                'name' => 'required|unique:categories,category_name,' . $request->id,
            ]);

            $category = Category::find($request->id);
            $category->category_name = $request->name;
            $category->description = $request->description;
            $category->save();
            return redirect('/category-showall')->with('success','Successfully Updated');
        }
    }
}
