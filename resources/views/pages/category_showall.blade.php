@extends('pages.navigation')
@section('category', 'active')
@section('content')

    <div class="card-header">
        <h4 class="header">Category</h4>
        <a href="" class="btn btn-success" data-toggle="modal" id="btn-add" data-target="#add">Add</a>
    </div>
    <div class="card-body">
        <div class="table-responsive">
            <table class="table table-striped" id="table-1">

                <thead>
                    <tr>
                        <th>Id</th>
                        <th>Category</th>
                        <th>Description</th>
                        <th class='action'>Action</th>
                    </tr>
                </thead>


                <tbody>
                    @foreach ($categories as $category)
                    <tr>
                        <td>{{$category->id}}</td>
                        <td>{{$category->category_name}}</td>
                        <td>{{$category->description}}</td>
                        <td>
                            <button class="btn btn-warning btn-edit" data-id="{{$category->id}}" data-cat="{{$category->category_name}}" data-des="{{$category->description}}" data-toggle="modal" data-target="#add">Edit</button>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>

    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"></script>
    <script>
        $(document).ready(function() {
            if (!@json($errors->isEmpty())) {
                console.log("erorr");
                $('#add').modal();
            }
        });

        $('#btn-add').click(function (e) {
            $('#id').val('');
            $('#name').val('');
            $('#description').val('');
            $('.text-danger').hide();
        });

        $('.btn-edit').click(function (e) {
            $('#id').val('');
            $('#name').val('');
            $('#description').val('');
            $('.text-danger').hide();

            var id=$(this).attr("data-id");
            var cat=$(this).attr("data-cat");
            var des=$(this).attr("data-des");

            $('#id').val(id);
            $('#name').val(cat);
            $('#description').val(des);
        });
    </script>

@endsection


{{-- modal start --}}
<div class="modal fade" id="add" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Category</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <!-- Main Content -->
                <form action="/category-store" method="post" class="needs-validation" novalidate="">
                    <div class="card-body form">
                        @csrf
                        <input type="text" name="id" id="id" value="{{old('id')}}" hidden>
                        <div class="row">
                            <div class="col-12">
                                <div class="form-group">
                                    <label>Category</label>
                                    <input type="text" class="form-control" name="name" id="name" value="{{old('name')}}" required>
                                    <span class="text-danger">
                                        @error('name')
                                            {{ $message }}
                                        @enderror
                                    </span>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-12">
                                <div class="form-group">
                                    <label>Description</label>
                                    <textarea class="form-control" name="description" id="description">{{old('description')}}</textarea>
                                </div>
                            </div>
                        </div>
                        <div align="right">
                            <button type="reset" class="btn btn-danger" id="reset">Reset</button>
                            <button type="submit" class="btn btn-success mr-1" id="submit">Submit</button>
                        </div>
                    </div>
                </form>

            </div>

        </div>
    </div>
</div>

{{-- modal end --}}
