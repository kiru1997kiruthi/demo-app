@extends('pages.navigation')
@section('files', 'active')
@section('content')

    <div class="card-header">
        <h4 class="header">Files</h4>
        <button class="btn btn-success" id="btn-add" data-toggle="modal" data-target="#add">Add</button>
    </div>
    <div class="card-body">
        <div class="table-responsive">
            <table class="table table-striped" id="table-1">
                <thead>
                    <tr>
                        <th>Id</th>
                        <th>Title</th>
                        <th>Category</th>
                        <th>File</th>
                        <th>Description</th>
                        <th class='action'>Action</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($files as $file)
                        <tr>
                            <td>{{ $file->id }}</td>
                            <td>{{ $file->title }}</td>
                            <td>{{ $file->category_name }}</td>
                            <td><a href="/files/{{ $file->file_name }}" target="_blank"
                                    rel="noopener noreferrer">{{ $file->file_name }}</a></td>
                            <td>{{ $file->description }}</td>
                            <td>
                                <button class="btn btn-warning btn-edit" data-id="{{ $file->id }}" data-title="{{ $file->title }}" data-cat="{{ $file->category_id }}" data-file_name="{{ $file->file_name }}" data-des="{{ $file->description }}" data-toggle="modal" data-target="#add">Edit</button>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>

    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"></script>
    <script>
        $(document).ready(function() {
            if (!@json($errors->isEmpty())) {
                console.log("erorr");
                $('#add').modal();
            }
        });

        $('#btn-add').click(function(e) {
            $('#id').val('');
            $('#title').val('');
            $('#category').val('');
            $('#file').val('');
            $('#description').val('');
            $('.text-danger').hide();
            $('#file_show').html('');
        });

        $('.btn-edit').click(function(e) {
            $('#id').val('');
            $('#title').val('');
            $('#category').val('');
            $('#file').val('');
            $('#description').val('');
            $('.text-danger').hide();
            $('#file_show').html('');

            var id=$(this).attr("data-id");
            var title=$(this).attr("data-title");
            var cat=$(this).attr("data-cat");
            var des=$(this).attr("data-des");
            var file=$(this).attr("data-file_name");

            console.log("category :- "+cat);

            $('#id').val(id);
            $('#title').val(title);
            $('#category').val(cat);
            $('#description').val(des);
            
            var html='';
            html+='<a href="/files/'+file+'" target="_blank" rel="noopener noreferrer">'+file+'</a>';
            $('#file_show').append(html);

        });
    </script>
@endsection


{{-- modal start --}}
<div class="modal fade" id="add" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">File</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <!-- Main Content -->
                <form action="/files-store" method="post" class="needs-validation" novalidate=""
                    enctype="multipart/form-data">
                    <div class="card-body form">
                        @csrf
                        <input type="text" name="id" id="id" value="{{old('id')}}" hidden>
                        <div class="row">
                            <div class="col-12">
                                <div class="form-group">
                                    <label>Title</label>
                                    <input type="text" class="form-control" name="title" id="title"
                                        value="{{ old('title') }}" required>
                                    <span class="text-danger">
                                        @error('title')
                                            {{ $message }}
                                        @enderror
                                    </span>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-12">
                                <div class="form-group">
                                    <label>Category</label>
                                    <select name="category" id="category" class="form-control" required>
                                        <option value="" disabled selected>Select Category</option>
                                        @foreach ($categories as $category)
                                            <option value="{{ $category->id }}" {{ $category->id == old('category') ? 'selected' : '' }}> {{ $category->category_name }}</option>
                                        @endforeach
                                    </select>
                                    <span class="text-danger">
                                        @error('category')
                                            {{ $message }}
                                        @enderror
                                    </span>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-12">
                                <div class="form-group">
                                    <div id="file_show"></div>
                                    <label>File</label>
                                    <input type="file" class="form-control" name="file" id="file" required>
                                    <span class="text-danger">
                                        @error('file')
                                            {{ $message }}
                                        @enderror
                                    </span>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-12">
                                <div class="form-group">
                                    <label>Description</label>
                                    <textarea class="form-control" name="description" id="description">{{ old('description') }}</textarea>
                                </div>
                            </div>
                        </div>
                        <div align="right">
                            <button type="reset" class="btn btn-danger" id="reset">Reset</button>
                            <button type="submit" class="btn btn-success mr-1" id="submit">Submit</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

{{-- modal end --}}
