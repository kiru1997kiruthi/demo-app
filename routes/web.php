<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\CategoryController;
use App\Http\Controllers\FilesController;
use Illuminate\Support\Facades\Auth;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth.login');
});

Route::get('/register', function () {
    return view('auth.register');
});

Route::group(['middleware' => ['auth']], function () { //' '
    Route::get('/dashboard', function () {return view('pages.dashboard');})->name('dashboard');
    Route::get('/category-showall',[CategoryController::class,'categoryShowall'])->name('category.showall');
    Route::post('/category-store',[CategoryController::class,'categoryStore'])->name('category.store');
    Route::get('/files-showall',[FilesController::class,'filesShowall'])->name('files.showall');
    Route::post('/files-store',[FilesController::class,'filesStore'])->name('files.store');
    Route::get('/log-out', function () {
        Auth::logout();
        return redirect('/');
    });
});

